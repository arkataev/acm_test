<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testArraySerialization()
    {
    	$data = ['my' => 'array', 'is' => 12345, [2,3,4,5, 'this is a string'], ['some' => 'named', 'hello world']];
        $this->json('POST', '/', ['data' => $data])->seeJsonEquals(['s_string' => serialize($data)]);
    }

	public function testObjectSerialization()
    {
	    $data  = new stdClass();
	    $data->name = 'Tester';
	    $data->work = 'Testing';
	    $data->tool = 'Test';

        $this->post('/', ['data' => $data])->seeJsonEquals(['s_string' => serialize($data)]);
    }

    public function testIntSerialization()
    {
	    $this->json('POST', '/', ['data' => 123456])->seeJsonEquals(['s_string' => serialize(123456)]);
    }

	public function testBoolSerialization()
	{
		$this->json('POST', '/', ['data' => True])->seeJsonEquals(['s_string' => serialize(True)]);
	}

	public function testStringSerialization()
	{
		$this->json('POST', '/', ['data' => 'hello world'])->seeJsonEquals(['s_string' => serialize('hello world')]);
	}

	public function testFloatSerialization()
	{
		$this->json('POST', '/', ['data' => 42.5])->seeJsonEquals(['s_string' => serialize(42.5)]);
	}
}
