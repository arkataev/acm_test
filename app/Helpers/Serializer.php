<?php

namespace App\Helpers;

use Exception;

class Serializer implements iSerializer
{
    public function do_serialize($input)
    {
	    $args = count(func_get_args());
	    if ( $args > 1) { throw new Exception("Serializator function requires exactly 1 parameter. {$args} given"); };
	    switch ($input) :
		    case is_array($input) :
			    $length = count($input);
			    $str = gettype($input)[0] . ':' . $length . ':{';
			    foreach ($input as $key => $value):
				    if (is_array($value)) {
					    $str .= gettype($key)[0] . ':' ;
					    $str .= !is_int($key) ? strlen($key) . ':' . '"' . $key . '"' . ';' : $key . ';';
					    $str .= $this->do_serialize($value);
				    }
				    else {
					    $str .= gettype($key)[0] . ':' ;
					    $str .= !is_int($key) ? strlen($key) . ':' . '"' . $key . '"' . ';' : $key . ';';
					    $str .= gettype($value)[0] . ':' ;
					    $str .= !is_int($value) ?  strlen($value) . ':' . '"' . $value . '"' . ";" : $value . ';';
				    }
			    endforeach;
			    $str .= '}';
			    break;
		    case is_object( $input ) :
			    $str = ucfirst(gettype($input)[0]) . ':' . strlen(get_class($input)) . ':' . '"' . get_class($input) . '"';
			    $str .= substr($this->do_serialize((array)$input), 1);
			    break;
		    case is_int( $input) || is_bool($input) || is_null($input) :
			    $str = gettype($input)[0] . ':' . $input . ';';
			    break;
		    case is_string( $input) :
			    $str = gettype($input)[0] . ':' . strlen($input). ':' . '"' . $input . '";';
			    break;
		    case is_float($input) :
			    $precision = ini_get('serialize_precision');
			    $str = gettype($input)[0] . ':' . round($input, $precision) . ';';
			    break;
		    case is_resource($input):
			    $str = 'i:0'; // resource is not serializable
			    break;
		    default:
			    return false;
	    endswitch;

	    return $str;
    }

    public function deserialize(string $input)
    {
	    // TODO: Implement unserializer() method.
    }
}
