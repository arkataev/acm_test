<?php
/**
 * @author reddev
 * @web https://bitbucket.org/arkataev
 * @date: 10.08.2016
 */

namespace App\Helpers;


interface iSerializer
{
	function do_serialize($input);

	function deserialize(string $input);
}