<?php

namespace App\Http\Controllers;

use App\Helpers\iSerializer;
use App\SerializedDataModel;
use Illuminate\Http\Request;


class SerializeController extends Controller
{
    public function index(iSerializer $serializer, Request $request)
    {
    	$data = $request->input('data');
		$model = new SerializedDataModel(['s_string' => $serializer->do_serialize($data)]);

	    return $model;
    }
}
