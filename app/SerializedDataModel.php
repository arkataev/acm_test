<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SerializedDataModel extends Model
{
	protected $fillable = ['s_string'];
}
