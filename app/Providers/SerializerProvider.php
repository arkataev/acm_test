<?php

namespace App\Providers;

use App\Helpers\Serializer;
use Illuminate\Support\ServiceProvider;

class SerializerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\iSerializer', function() {
        	return new Serializer();
        });
    }
}
